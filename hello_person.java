import java.util.Random;

class hello_person{
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String BLUE = "\033[0;34m";    // BLUE
    public static final String PURPLE = "\033[0;35m";  // PURPLE
    public static final String CYAN = "\033[0;36m";    // CYAN
    public static final String WHITE = "\033[0;37m";   // WHITE

    // Bold
    public static final String BLACK_BOLD = "\033[1;30m";  // BLACK
    public static final String RED_BOLD = "\033[1;31m";    // RED
    public static final String GREEN_BOLD = "\033[1;32m";  // GREEN
    public static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW
    public static final String BLUE_BOLD = "\033[1;34m";   // BLUE
    public static final String PURPLE_BOLD = "\033[1;35m"; // PURPLE
    public static final String CYAN_BOLD = "\033[1;36m";   // CYAN
    public static final String WHITE_BOLD = "\033[1;37m";  // WHITE

    // Underline
    public static final String BLACK_UNDERLINED = "\033[4;30m";  // BLACK
    public static final String RED_UNDERLINED = "\033[4;31m";    // RED
    public static final String GREEN_UNDERLINED = "\033[4;32m";  // GREEN
    public static final String YELLOW_UNDERLINED = "\033[4;33m"; // YELLOW
    public static final String BLUE_UNDERLINED = "\033[4;34m";   // BLUE
    public static final String PURPLE_UNDERLINED = "\033[4;35m"; // PURPLE
    public static final String CYAN_UNDERLINED = "\033[4;36m";   // CYAN
    public static final String WHITE_UNDERLINED = "\033[4;37m";  // WHITE

    // Background
    public static final String BLACK_BACKGROUND = "\033[40m";  // BLACK
    public static final String RED_BACKGROUND = "\033[41m";    // RED
    public static final String GREEN_BACKGROUND = "\033[42m";  // GREEN
    public static final String YELLOW_BACKGROUND = "\033[43m"; // YELLOW
    public static final String BLUE_BACKGROUND = "\033[44m";   // BLUE
    public static final String PURPLE_BACKGROUND = "\033[45m"; // PURPLE
    public static final String CYAN_BACKGROUND = "\033[46m";   // CYAN
    public static final String WHITE_BACKGROUND = "\033[47m";  // WHITE

    // High Intensity
    public static final String BLACK_BRIGHT = "\033[0;90m";  // BLACK
    public static final String RED_BRIGHT = "\033[0;91m";    // RED
    public static final String GREEN_BRIGHT = "\033[0;92m";  // GREEN
    public static final String YELLOW_BRIGHT = "\033[0;93m"; // YELLOW
    public static final String BLUE_BRIGHT = "\033[0;94m";   // BLUE
    public static final String PURPLE_BRIGHT = "\033[0;95m"; // PURPLE
    public static final String CYAN_BRIGHT = "\033[0;96m";   // CYAN
    public static final String WHITE_BRIGHT = "\033[0;97m";  // WHITE

    // Bold High Intensity
    public static final String BLACK_BOLD_BRIGHT = "\033[1;90m"; // BLACK
    public static final String RED_BOLD_BRIGHT = "\033[1;91m";   // RED
    public static final String GREEN_BOLD_BRIGHT = "\033[1;92m"; // GREEN
    public static final String YELLOW_BOLD_BRIGHT = "\033[1;93m";// YELLOW
    public static final String BLUE_BOLD_BRIGHT = "\033[1;94m";  // BLUE
    public static final String PURPLE_BOLD_BRIGHT = "\033[1;95m";// PURPLE
    public static final String CYAN_BOLD_BRIGHT = "\033[1;96m";  // CYAN
    public static final String WHITE_BOLD_BRIGHT = "\033[1;97m"; // WHITE

    // High Intensity backgrounds
    public static final String BLACK_BACKGROUND_BRIGHT = "\033[0;100m";// BLACK
    public static final String RED_BACKGROUND_BRIGHT = "\033[0;101m";// RED
    public static final String GREEN_BACKGROUND_BRIGHT = "\033[0;102m";// GREEN
    public static final String YELLOW_BACKGROUND_BRIGHT = "\033[0;103m";// YELLOW
    public static final String BLUE_BACKGROUND_BRIGHT = "\033[0;104m";// BLUE
    public static final String PURPLE_BACKGROUND_BRIGHT = "\033[0;105m"; // PURPLE
    public static final String CYAN_BACKGROUND_BRIGHT = "\033[0;106m";  // CYAN
    public static final String WHITE_BACKGROUND_BRIGHT = "\033[0;107m";   // WHITE

    public static String[] craigsColours={ ANSI_CYAN, ANSI_GREEN, ANSI_RED, RED_BOLD_BRIGHT, GREEN_BOLD_BRIGHT, YELLOW_BOLD_BRIGHT, BLUE_BOLD_BRIGHT, PURPLE_BOLD_BRIGHT, WHITE_BOLD_BRIGHT};
    public static String[] craigsBackgrounds={RED_BACKGROUND_BRIGHT, PURPLE_BACKGROUND_BRIGHT, YELLOW_BACKGROUND_BRIGHT,GREEN_BACKGROUND_BRIGHT, BLUE_BACKGROUND_BRIGHT};
    //windows command prompt does not show colors, it works in GitBash, hopefully Z-shell as well :)
    
    public static void main(final String[] a) {
        String s = String.join(" ",a);
        /*if (a.length!=0){
            System.out.println(craigify(s));
            
        }*/
        
        if (a.length != 0) {
            try {// does not work for ø, but for å and æ.
                if (a.length==1){
                    if(a[0].toLowerCase().equals("craig")){
                        System.out.printf("Hello %s %s %s, Your name is %s %d %s characters long and starts with a %s %s %s %n", ANSI_GREEN,
                            new String(craigify(s).getBytes("Cp1252")), ANSI_RESET, ANSI_RED, s.length(),ANSI_RESET, ANSI_CYAN , s.substring(0, 1), ANSI_RESET);
                    }
                    else{
                        System.out.printf("Hello %s %s %s, Your name is %s %d %s characters long and starts with a %s %s %s %n", ANSI_GREEN,
                        new String(s.getBytes("Cp1252")), ANSI_RESET, ANSI_RED, s.length(),ANSI_RESET, ANSI_CYAN , s.substring(0, 1), ANSI_RESET);
                    }
                }else{
                    if(a[0].toLowerCase().equals("craig")){
                        System.out.printf("Hello %s %s %s, Your name is %s %d %s characters long and starts with a %s %s %s %n", ANSI_GREEN,
                        new String(craigify(s).getBytes("Cp1252")), ANSI_RESET, ANSI_RED, s.length()- a.length+1,ANSI_RESET, ANSI_CYAN , s.substring(0, 1), ANSI_RESET);
                    }else{
                        System.out.printf("Hello %s %s %s, Your name is %s %d %s characters long and starts with a %s %s %s %n", ANSI_GREEN,
                        new String(s.getBytes("Cp1252")), ANSI_RESET, ANSI_RED, s.length()- a.length+1,ANSI_RESET, ANSI_CYAN , s.substring(0, 1), ANSI_RESET);
                    }
                }
            } catch (final Exception e) {
                System.out.print(e);
            }
        }else{
            System.out.printf("Hello, you have not entered a name, please enter a name %n");
        }
    }
    static String craigify(String s){
        String st="";
        for(int i =0; i<s.length();i++){
            st+= craigsColours[new Random().nextInt(craigsColours.length)] +s.charAt(i) + craigsBackgrounds[new Random().nextInt(craigsBackgrounds.length)] ;
        }
        return st+ANSI_RESET;
    }

}